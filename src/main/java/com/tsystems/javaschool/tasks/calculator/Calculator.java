package com.tsystems.javaschool.tasks.calculator;

import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(final String statement) {
        if (statement == null || statement.isEmpty()) {
            return null;
        }
        final Stack<Double> output = new Stack<>();
        final Stack<Character> operators = new Stack<>();
        int currentTokenStarts = 0;
        while (currentTokenStarts < statement.length()) {
            if (isOperator(statement.charAt(currentTokenStarts))) {
                final char op1 = statement.charAt(currentTokenStarts);
                while (!operators.empty() && operators.peek() != '(' && operatorPriority(op1) <= operatorPriority(operators.peek())) {
                    if (output.size() < 2) return null;
                    final double arg2 = output.pop();
                    final double arg1 = output.pop();
                    final Double operatorResult = applyOperator(arg1, arg2, operators.pop());
                    if (operatorResult == null) return null;
                    output.push(operatorResult);
                }
                operators.push(op1);
                currentTokenStarts++;
            } else if (statement.charAt(currentTokenStarts) == '(') {
                operators.push('(');
                currentTokenStarts++;
            } else if (statement.charAt(currentTokenStarts) == ')') {
                while (!operators.empty() && operators.peek() != '(') {
                    if (output.size() < 2) return null;
                    final double arg2 = output.pop();
                    final double arg1 = output.pop();
                    final Double operatorResult = applyOperator(arg1, arg2, operators.pop());
                    if (operatorResult == null) return null;
                    output.push(operatorResult);
                }
                if (operators.empty()) {
                    return null;
                }
                operators.pop();
                currentTokenStarts++;
            } else {
                int currentTokenEnds = currentTokenStarts + 1;
                while (currentTokenEnds < statement.length()
                        && statement.charAt(currentTokenEnds) != ')'
                        && statement.charAt(currentTokenEnds) != '('
                        && !isOperator(statement.charAt(currentTokenEnds))) {
                    currentTokenEnds++;
                }
                final String token = statement.substring(currentTokenStarts, currentTokenEnds);
                try {
                    output.push(Double.parseDouble(token));
                } catch (final NumberFormatException e) {
                    return null;
                }
                currentTokenStarts = currentTokenEnds;
            }
        }
        while (!operators.empty()) {
            if (output.size() < 2) return null;
            final double arg2 = output.pop();
            final double arg1 = output.pop();
            final Double operatorResult = applyOperator(arg1, arg2, operators.pop());
            if (operatorResult == null) return null;
            output.push(operatorResult);
        }
        if (output.size() != 1) return null;

        final double result = output.pop();
        if ((result == Math.floor(result)) && !Double.isInfinite(result)) {
            return String.format(Locale.US, "%d", (long) result);
        } else {
            return String.format(Locale.US, "%s", result);
        }
    }

    private boolean isOperator(final char op) {
        return op == '+' || op == '-' || op == '*' || op == '/';
    }

    private int operatorPriority(final char op) {
        switch (op) {
            case '+':
                return 1;
            case '-':
                return 1;
            case '*':
                return 2;
            case '/':
                return 2;
            default:
                throw new IllegalArgumentException("Unrecognized operator '" + op + "'");
        }
    }

    private Double applyOperator(final double arg1, final double arg2, final char operator) {
        switch (operator) {
            case '+':
                return arg1 + arg2;
            case '-':
                return arg1 - arg2;
            case '*':
                return arg1 * arg2;
            case '/':
                return arg2 == 0 ? null : arg1 / arg2;
            default:
                throw new IllegalArgumentException("Unrecognized operator '" + operator + "'");
        }
    }
}
