package com.tsystems.javaschool.tasks.pyramid;

import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws CannotBuildPyramidException if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(final List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.isEmpty())
            throw new CannotBuildPyramidException();

        final int n = inputNumbers.size();

        final double d = Math.sqrt(1 + 8 * n);
        if (Math.ceil(d) != Math.floor(d)){
            throw new CannotBuildPyramidException();}
        final int height = (int) (d - 1) / 2;
        final int width = 2 * height - 1;
        final int[][] result = new int[height][width];

        final Iterator<Integer> iterator = inputNumbers.stream().sorted().iterator();

        for (int i = 0; i < height; i++) {
            final int countNumInRow = i + 1;
            final int leftIndex = height - i - 1;
            for (int j = 0, index = leftIndex; j < countNumInRow; j++, index += 2) {
                result[i][index] = iterator.next();
            }
        }
        return result;
    }
}
